// Инициализация
const serialPortInit = require('serialport');
var io = require('socket.io').listen(3000);
var serialPort = new serialPortInit("/dev/ttyUSB0", {
    baudRate: 9600,
    parser: new serialPortInit.parsers.Readline("\n")
});
clickElementsAddEvent(0);

//Обновить список последовательных портов
function updateSerialPortList() {
    //var SerialPort = require('serialport');

    var combo = document.getElementById("serial_ports");
    combo.options.length = 0;

    serialPortInit.list(function (err, ports) {
        ports.forEach(function (port) {
            console.log(port.comName);
            console.log(port.pnpId);
            console.log(port.manufacturer);

            var option = document.createElement("option");
            option.text = port.comName;
            option.value = port.comName;
            combo.add(option, null);
        });
    });
}
// Сменить порт
function changePort(){
    // var portName = document.getElementById('serial_ports').value;
    // serialPort = new serialPortInit(portName, {
    //     baudRate: 9600,
    //     parser: new serialPortInit.parsers.Readline("\n")
    // });
    // console.log('port changed'+portName);
}


io.sockets.on('connection', function (socket) {
    socket.on('message', function (msg) {
        console.log(msg);
    });

    socket.on('disconnected', function () {
        console.log('disconnected');
    });
});


function connect() {
    //babbler.connect(document.getElementById('serial_ports').value);
}

function cmdSend(message) {
    console.log(message);
    serialPort.write(message);
}

var readData = '';
serialPort.on('open', function () {
    console.log('open');
    serialPort.on('data', function (data) {
        console.log(data);
        data = data.toString().replace(/(?:\r\n|\r|\n)/g, '<br/>');
        //document.getElementById("message_area").firstChild.nodeValue += data;
        var messageArea = document.getElementById("message_area");
        messageArea.firstElementChild.innerHTML += data;
        //readData += data.toString();
        //io.sockets.emit('message', data);
        messageArea.scrollTop = messageArea.scrollHeight;
    });
});

// Создаем обработчик кликов по элементам списка комманд
// начиная с элемента number
function clickElementsAddEvent(number){
    var clickElements = commandList.childNodes;
    for (i = number; i < clickElements.length; i++) {
        clickElements[i].addEventListener('click', function() {
        var elementValue = this.innerHTML;
        cmdSend(elementValue+'\n');
        //this.style.color = "green";
        });
    }
}


// Очистка текстового поля
document.getElementById("clearButton").onclick = function(e) {
    document.getElementById("message_area").firstElementChild.innerHTML = "";
}

// Добавление комманды в список
document.getElementById("addCommandButton").onclick = function(e) {
    console.log('button pressed');
    let p = document.createElement('p');
    p.innerHTML = document.getElementById('addCommandInput').value;
    var commandList = document.getElementById("commandList");
    var number = commandList.childNodes.length;
    commandList.append(p);
    clickElementsAddEvent(number);
}

// Событие закрытия порта
serialPort.on('close', portCloseAcion);

function portCloseAcion(){
    console.log("Port closed");
    serialPort = new serialPortInit("/dev/ttyUSB1", {
        baudRate: 9600,
        parser: new serialPortInit.parsers.Readline("\n")
    });
}